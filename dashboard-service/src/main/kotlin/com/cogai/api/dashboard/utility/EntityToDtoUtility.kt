package com.cogai.api.dashboard.utility

import com.cogai.api.dashboard.dto.CameraNotificationSettingsResponseBody
import com.cogai.api.dashboard.dto.CameraResponseBody
import com.cogai.api.dashboard.dto.FiltersResponseBody
import com.cogai.api.dashboard.dto.NotificationSettingsResponseBody
import com.cogai.api.dashboard.dto.UserResponseBody
import com.cogai.api.dashboard.dto.ZoneResponseBody
import com.cogai.api.dashboard.dto.ZoneSettingsResponseBody
import com.cogai.api.dashboard.entity.Camera
import com.cogai.api.dashboard.entity.CameraNotificationSettings
import com.cogai.api.dashboard.entity.NotificationSettings
import com.cogai.api.dashboard.entity.User
import com.cogai.api.dashboard.entity.Zone
import com.cogai.api.dashboard.entity.ZoneSettings

fun Zone.toZoneResponseBody() = ZoneResponseBody(
    id = this.id,
    name = this.name,
    settings = this.settings.toZoneSettingsResponseBody(),
    createdBy = this.createdBy,
    modifiedBy = this.modifiedBy,
    createdAt = this.createdAt,
    modifiedAt = this.modifiedAt
)

fun ZoneSettings.toZoneSettingsResponseBody() = ZoneSettingsResponseBody(
    violationDistance = this.violationDistance,
    violationDuration = this.violationDuration,
    motion = this.motion,
    initializeIn = this.initializeIn
)

fun Camera.toCameraResponseBody() = CameraResponseBody(
    id = this.id,
    name = this.name,
    zoneId = this.zoneId,
    link = this.link,
    rawLink = this.rawLink,
    resolution = this.resolution,
    type = this.type,
    notificationSettingEnabled = this.notificationSettingEnabled,
    createdBy = this.createdBy,
    modifiedBy = this.modifiedBy,
    createdAt = this.createdAt,
    modifiedAt = this.modifiedAt
)

fun User.toUserResponseBody() = UserResponseBody(
    id = this.id,
    email = this.email,
    firstName = this.firstName,
    lastName = this.lastName,
    userName = this.userName,
    persona = this.persona,
    mobileNumber = this.mobileNumber,
    userProfileImage = this.userProfileImage,
    createdBy = this.createdBy,
    modifiedBy = this.modifiedBy,
    createdAt = this.createdAt,
    modifiedAt = this.modifiedAt
)

fun NotificationSettings.toNotificationSettingsResponseBody() = NotificationSettingsResponseBody(
    id = this.id,
    userId = this.userId,
    filters = FiltersResponseBody(
        minNumOfViolations = this.filters.minNumOfViolations,
        maxNumOfViolations = this.filters.maxNumOfViolations
    ),
    action = this.action,
    zoneId = this.zoneId,
    cameras = this.cameras.map { it.toCameraResponseBody() },
    createdBy = this.createdBy,
    modifiedBy = this.modifiedBy,
    createdAt = this.createdAt,
    modifiedAt = this.modifiedAt
)

fun CameraNotificationSettings.toCameraNotificationSettingsResponseBody() = CameraNotificationSettingsResponseBody(
    id = this.id,
    zoneId = this.zoneId,
    cameraId = this.cameraId,
    filters = FiltersResponseBody(
        minNumOfViolations = this.minNumOfViolations,
        maxNumOfViolations = this.maxNumOfViolations
    ),
    action = this.action,
    userId = this.userId,
    createdBy = this.createdBy,
    modifiedBy = this.modifiedBy,
    createdAt = this.createdAt,
    modifiedAt = this.modifiedAt
)
