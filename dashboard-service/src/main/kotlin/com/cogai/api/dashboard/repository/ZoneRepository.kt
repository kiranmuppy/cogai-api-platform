package com.cogai.api.dashboard.repository

import com.cogai.api.dashboard.configuration.OffsetLimitPageable
import com.cogai.api.dashboard.entity.Zone
import org.springframework.data.domain.Page
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ZoneRepository : MongoRepository<Zone, String> {

    fun findOneById(id: String): Zone

    fun deleteZoneById(id: String)

    fun findAllByName(name: String, offsetLimitPageable: OffsetLimitPageable): Page<Zone>
}
