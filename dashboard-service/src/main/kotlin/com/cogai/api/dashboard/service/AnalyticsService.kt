package com.cogai.api.dashboard.service

import com.cogai.api.dashboard.dto.RawStatsResponse
import com.cogai.api.dashboard.entity.RawStats
import com.cogai.api.dashboard.repository.AnalyticsRepository
import com.cogai.api.dashboard.repository.RawStatsRepository
import org.springframework.stereotype.Service
import java.math.BigInteger
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

@Service
class AnalyticsService(
    private val analyticsRepository: AnalyticsRepository,
    private val rawStatsRepository: RawStatsRepository
) {

    fun getLatestStats(time: LocalDateTime, zoneId: String?): List<RawStats> {
        return if (zoneId.isNullOrEmpty()) {
            return rawStatsRepository.findAllByFromTimeIsGreaterThanEqualOrderByFromTime(time)
        } else {
            rawStatsRepository.findAllByFromTimeIsGreaterThanEqualAndZoneIdOrderByFromTime(time, zoneId)
        }
    }

    fun getLatestStatsByInterval(zoneId: String?): BigInteger {
        val currentDateTime = LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS)
        val stats = if (zoneId.isNullOrEmpty()) {
            rawStatsRepository.findAllRawStatsByInterval(currentDateTime.minusMinutes(10), currentDateTime)
        } else {
            rawStatsRepository.findAllRawStatsByIntervalByZone(currentDateTime.minusMinutes(10), currentDateTime, zoneId)
        }
        return stats.sumOf { it.stats.violations }
    }

    fun getPreviousStatsAggregate(fromTime: LocalDateTime, toTime: LocalDateTime, interval: Int, zoneId: String?): List<RawStatsResponse> {
        return if (zoneId.isNullOrEmpty()) {
            analyticsRepository.findPreviousStatsAggregateWithOutZoneId(fromTime, toTime, interval)
        } else {
            analyticsRepository.findPreviousStatsAggregateSpecificZone(fromTime, toTime, interval, zoneId)
        }
    }

    fun getNetStatsForToday(zoneId: String?): BigInteger {
        val fromTime = LocalDate.now().atStartOfDay()
        val toTime = LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS)
        val stats = if (zoneId.isNullOrEmpty()) {
            rawStatsRepository.findAllRawStatsByInterval(fromTime, toTime)
        } else {
            rawStatsRepository.findAllRawStatsByIntervalByZone(fromTime, toTime, zoneId)
        }
        return stats.sumOf { it.stats.violations }
    }
}
