package com.cogai.api.dashboard

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching

@SpringBootApplication
@EnableCaching
class DashboardService
fun main(args: Array<String>) {
    runApplication<DashboardService>(*args)
}
