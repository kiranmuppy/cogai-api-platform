package com.cogai.api.dashboard.service

import com.cogai.api.dashboard.configuration.OffsetLimitPageable
import com.cogai.api.dashboard.dto.CameraRequestBody
import com.cogai.api.dashboard.dto.CameraResponseBody
import com.cogai.api.dashboard.entity.Camera
import com.cogai.api.dashboard.exception.ResourceNotFoundException
import com.cogai.api.dashboard.repository.CameraRepository
import com.cogai.api.dashboard.repository.ZoneRepository
import com.cogai.api.dashboard.utility.formattedDate
import com.cogai.api.dashboard.utility.logger
import com.cogai.api.dashboard.utility.toCameraEntity
import com.cogai.api.dashboard.utility.toCameraResponseBody
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class CameraService(
    private val cameraRepository: CameraRepository,
    private val zoneRepository: ZoneRepository
) {

    private val log = logger()

    fun createCamera(requestBody: CameraRequestBody): CameraResponseBody {
        return cameraRepository.save(requestBody.toCameraEntity()).toCameraResponseBody()
    }

    fun getCamera(id: String): CameraResponseBody {
        return try {
            cameraRepository.findOneById(id).toCameraResponseBody()
        } catch (e: ResourceNotFoundException) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    fun deleteCamera(id: String) {
        return try {
            cameraRepository.deleteCameraById(id)
        } catch (e: ResourceNotFoundException) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    fun getListOfCameras(name: String, offset: Int, limit: Int): Page<CameraResponseBody> {
        return if (name.isEmpty()) {
            cameraRepository.findAll(OffsetLimitPageable(offset, limit, Sort.unsorted())).map { it.toCameraResponseBody() }
        } else {
            cameraRepository.findAllByName(name, OffsetLimitPageable(offset, limit, Sort.unsorted())).map { it.toCameraResponseBody() }
        }
    }

    fun getListOfCamerasByZoneId(id: String): List<CameraResponseBody> {
        return cameraRepository.findAllByZoneId(id).map { it.toCameraResponseBody() }
    }

    fun updateCamera(id: String, requestBody: CameraRequestBody): CameraResponseBody {
        try {
            val cameraEntity = cameraRepository.findOneById(id)
            val updatedCameraEntity = Camera(
                id = cameraEntity.id,
                name = requestBody.name,
                zoneId = cameraEntity.zoneId,
                link = requestBody.link,
                rawLink = requestBody.rawLink,
                resolution = requestBody.resolution,
                type = requestBody.type,
                createdAt = cameraEntity.createdAt,
                createdBy = cameraEntity.createdBy,
                modifiedBy = requestBody.modifiedBy,
                modifiedAt = LocalDateTime.now().formattedDate()
            )
            return cameraRepository.save(updatedCameraEntity).toCameraResponseBody()
        } catch (e: ResourceNotFoundException) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }
}
