package com.cogai.api.dashboard.authorization

import com.cogai.api.dashboard.authorization.jwt.JwtTokenUtil
import com.cogai.api.dashboard.authorization.jwt.JwtUserDetailsService
import com.cogai.api.dashboard.dto.AuthenticationResponse
import com.cogai.api.dashboard.dto.JwtRequestBody
import com.cogai.api.dashboard.repository.UserRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class AuthenticationController(
    private val userRepository: UserRepository,
    private val authenticationManager: AuthenticationManager,
    private val jwtUserDetailsService: JwtUserDetailsService,
    private val jwtTokenUtil: JwtTokenUtil
) {

    @PostMapping("authenticate")
    fun authenticateUser(
        @RequestBody requestBody: JwtRequestBody
    ): ResponseEntity<AuthenticationResponse> {
        authenticationManager.authenticate(UsernamePasswordAuthenticationToken(requestBody.email, requestBody.password))
        val user = userRepository.findByEmail(requestBody.email) ?: throw UsernameNotFoundException("User not found")
        val userDetails = jwtUserDetailsService.loadUserByUsername(user.email)
        val token = jwtTokenUtil.generateToken(userDetails)
        val userInfo = userRepository.findByEmail(requestBody.email)
        if (userInfo != null) {
            return ResponseEntity(
                AuthenticationResponse(jwtToken = "Bearer $token", user = userInfo),
                HttpStatus.OK
            )
        } else {
            throw UsernameNotFoundException("User not found")
        }
    }
}
