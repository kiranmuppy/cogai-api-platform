package com.cogai.api.dashboard.dto

data class CameraRequestBody(
    val name: String,
    val zoneId: String,
    val link: String,
    val rawLink: String,
    val resolution: String,
    val type: String,
    val createdBy: String,
    val modifiedBy: String
)

data class ZoneRequestBody(
    val name: String,
    val settings: ZoneSettingsRequestBody,
    val createdBy: String,
    val modifiedBy: String
)

data class ZoneSettingsRequestBody(
    val violationDistance: String,
    val violationDuration: String,
    val motion: Int = 0,
    val initializeIn: Int = 0
)

data class UserRequestBody(
    val email: String,
    val firstName: String,
    val lastName: String,
    val userName: String,
    val persona: String,
    val mobileNumber: String,
    val userProfileImage: String,
    val password: String,
    val createdBy: String,
    val modifiedBy: String
)

data class NotificationSettingsRequestBody(
    val userId: String,
    val zoneId: String,
    val selectedCameras: Map<String, Boolean>,
    val action: String,
    val filters: FiltersRequestBody,
    val createdBy: String,
    val modifiedBy: String
)

data class FiltersRequestBody(
    val minNoOfViolations: Int,
    val maxNoOfViolations: Int
)

data class JwtRequestBody(
    val email: String,
    val password: String
)

data class CameraNotificationSettingsRequestBody(
    val userId: String,
    val zoneId: String,
    val cameraId: String,
    val action: String,
    val filters: FiltersRequestBody,
    val createdBy: String,
    val modifiedBy: String
)

data class UpdateCameraNotificationSettingsRequestBody(
    val userId: String,
    val action: String,
    val filters: FiltersRequestBody,
    val createdBy: String,
    val modifiedBy: String
)

data class PreviousAnalyticsRequestBody(
    val fromTime: String,
    val toTime: String,
    val timeZone: String,
    val interval: Int = 900000,
    val zoneId: String?
)

data class LatestAnalyticsRequestBody(
    val time: String,
    val timeZone: String,
    val zoneId: String?
)
