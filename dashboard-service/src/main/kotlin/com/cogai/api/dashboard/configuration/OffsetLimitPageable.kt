package com.cogai.api.dashboard.configuration

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort

class OffsetLimitPageable(private val offset: Int, limit: Int, sort: Sort) :
    PageRequest(offset, limit, sort) {
    private val pageNumber: Int = offset / limit
    override fun getOffset() = offset.toLong()
    override fun getPageNumber() = pageNumber
}
