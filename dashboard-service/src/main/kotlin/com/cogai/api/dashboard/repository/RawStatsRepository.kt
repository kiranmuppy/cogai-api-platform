package com.cogai.api.dashboard.repository

import com.cogai.api.dashboard.entity.RawStats
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class RawStatsRepository(
    private val mongoTemplate: MongoTemplate
) {

    fun findAllByFromTimeIsGreaterThanEqualOrderByFromTime(fromTime: LocalDateTime): List<RawStats> {
        val query = Query()
        query.addCriteria(Criteria.where("from_time").gte(fromTime)).with(Sort.by(Sort.Direction.ASC, "from_time"))
        return mongoTemplate.find(query, RawStats::class.java)
    }

    fun findAllByFromTimeIsGreaterThanEqualAndZoneIdOrderByFromTime(fromTime: LocalDateTime, zoneId: String): List<RawStats> {
        val query = Query()
        query.addCriteria(Criteria.where("from_time").gte(fromTime).and("zone_id").isEqualTo(zoneId)).with(Sort.by(Sort.Direction.ASC, "from_time"))
        return mongoTemplate.find(query, RawStats::class.java)
    }

    fun findAllRawStats(): List<RawStats> {
        return mongoTemplate.findAll(RawStats::class.java)
    }

    fun findAllRawStatsByIntervalByZone(fromTime: LocalDateTime, toTime: LocalDateTime, zoneId: String): List<RawStats> {
        val query = Query()
        query.addCriteria(
            Criteria.where("from_time")
                .gte(fromTime)
                .lte(toTime)
                .and("zone_id").isEqualTo(zoneId)
        )
        return mongoTemplate.find(query, RawStats::class.java)
    }
    
    fun findAllRawStatsByInterval(fromTime: LocalDateTime, toTime: LocalDateTime): List<RawStats> {
        val query = Query()
        query.addCriteria(
            Criteria.where("from_time")
                .gte(fromTime)
                .lte(toTime)
        )
        return mongoTemplate.find(query, RawStats::class.java)
    }
}
