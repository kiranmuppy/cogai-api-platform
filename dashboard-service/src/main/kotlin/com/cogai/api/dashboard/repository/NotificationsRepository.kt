package com.cogai.api.dashboard.repository

import com.cogai.api.dashboard.entity.Notifications
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface NotificationsRepository : MongoRepository<Notifications, String> {

}
