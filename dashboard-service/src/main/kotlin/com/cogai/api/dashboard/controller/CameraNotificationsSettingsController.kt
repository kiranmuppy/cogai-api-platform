package com.cogai.api.dashboard.controller

import com.cogai.api.dashboard.dto.CameraNotificationSettingsRequestBody
import com.cogai.api.dashboard.dto.CameraNotificationSettingsResponseBody
import com.cogai.api.dashboard.dto.UpdateCameraNotificationSettingsRequestBody
import com.cogai.api.dashboard.service.CameraNotificationSettingsService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.NotBlank

@RestController
@CrossOrigin(origins = ["*"])
class CameraNotificationsSettingsController(
    private val cameraNotificationSettingsService: CameraNotificationSettingsService
) {

    @PostMapping("camera-notification-setting/")
    fun createCameraNotificationsSetting(@RequestBody requestBody: CameraNotificationSettingsRequestBody): ResponseEntity<CameraNotificationSettingsResponseBody> {
        return ResponseEntity(
            cameraNotificationSettingsService.createCameraNotificationSettings(requestBody),
            HttpStatus.CREATED
        )
    }

    @DeleteMapping("camera-notification-setting/{id}")
    fun deleteCameraNotificationsSetting(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<*> {
        cameraNotificationSettingsService.deleteCameraNotificationSettings(id)
        return ResponseEntity<Any>(HttpStatus.NO_CONTENT)
    }

    @GetMapping("camera-notification-setting/{id}")
    fun getCameraNotificationsSetting(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<CameraNotificationSettingsResponseBody> {
        return ResponseEntity(
            cameraNotificationSettingsService.getCameraNotificationSettingsById(id),
            HttpStatus.OK
        )
    }

    @PutMapping("camera-notification-setting/{id}")
    fun updateCameraNotificationsSetting(
        @NotBlank @PathVariable id: String,
        @RequestBody requestBody: UpdateCameraNotificationSettingsRequestBody
    ): ResponseEntity<*> {
        return ResponseEntity(
            cameraNotificationSettingsService.updateCameraNotificationSettings(id, requestBody),
            HttpStatus.OK
        )
    }

    @GetMapping("camera-notification-setting")
    fun getCameraNotificationSettings(
        @RequestParam(defaultValue = "0") offset: Int,
        @RequestParam(defaultValue = "10") limit: Int
    ): ResponseEntity<*> {
        return ResponseEntity(
            cameraNotificationSettingsService.getListOfCameraNotificationSettings(offset, limit),
            HttpStatus.OK
        )
    }

    @GetMapping("camera-notification-setting/camera")
    fun getNotificationSettingsByCamera(
        @RequestParam cameraId: String
    ): ResponseEntity<*> {
        return ResponseEntity(
            cameraNotificationSettingsService.getNotificationSettingsByCamera(cameraId),
            HttpStatus.OK
        )
    }
}
