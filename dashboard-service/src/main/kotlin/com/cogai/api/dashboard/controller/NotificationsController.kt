package com.cogai.api.dashboard.controller

import com.cogai.api.dashboard.dto.CameraRequestBody
import com.cogai.api.dashboard.dto.CameraResponseBody
import com.cogai.api.dashboard.service.CameraService
import com.cogai.api.dashboard.service.NotificationsService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.NotBlank

@RestController
@CrossOrigin(origins = ["*"])
class NotificationsController(
    private val notificationsService: NotificationsService
) {
    
    
    @GetMapping("notifications")
    fun getListOfNotifications(
        @RequestParam(defaultValue = "0") offset: Int,
        @RequestParam(defaultValue = "10") limit: Int
    ): ResponseEntity<*> {
        return ResponseEntity(
            notificationsService.getListOfNotifications(offset, limit),
            HttpStatus.OK
        )
    }
}
