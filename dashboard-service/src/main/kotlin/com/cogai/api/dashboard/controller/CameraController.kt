package com.cogai.api.dashboard.controller

import com.cogai.api.dashboard.dto.CameraRequestBody
import com.cogai.api.dashboard.dto.CameraResponseBody
import com.cogai.api.dashboard.service.CameraService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.NotBlank

@RestController
@CrossOrigin(origins = ["*"])
class CameraController(
    private val cameraService: CameraService
) {

    @PostMapping("camera")
    fun createCamera(@RequestBody requestBody: CameraRequestBody): ResponseEntity<CameraResponseBody> {
        return ResponseEntity(
            cameraService.createCamera(requestBody),
            HttpStatus.CREATED
        )
    }

    @GetMapping("camera/{id}")
    fun getCamera(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<*> {
        return ResponseEntity(
            cameraService.getCamera(id),
            HttpStatus.OK
        )
    }

    @DeleteMapping("camera/{id}")
    fun deleteCamera(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<*> {
        cameraService.deleteCamera(id)
        return ResponseEntity<Any>(HttpStatus.NO_CONTENT)
    }

    @PutMapping("camera/{id}")
    fun updateCamera(
        @NotBlank @PathVariable id: String,
        @RequestBody requestBody: CameraRequestBody
    ): ResponseEntity<CameraResponseBody> {
        return ResponseEntity(
            cameraService.updateCamera(id, requestBody),
            HttpStatus.OK
        )
    }

    @GetMapping("camera/zone/{id}")
    fun getListOfCamerasByZoneId(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<*> {
        return ResponseEntity(
            cameraService.getListOfCamerasByZoneId(id),
            HttpStatus.OK
        )
    }

    @GetMapping("camera")
    fun getListOfCameras(
        @RequestParam(defaultValue = "0") offset: Int,
        @RequestParam(defaultValue = "10") limit: Int,
        @RequestParam(defaultValue = "") name: String
    ): ResponseEntity<*> {
        return ResponseEntity(
            cameraService.getListOfCameras(name, offset, limit),
            HttpStatus.OK
        )
    }
}
