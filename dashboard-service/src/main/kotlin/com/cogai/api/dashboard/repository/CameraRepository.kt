package com.cogai.api.dashboard.repository

import com.cogai.api.dashboard.configuration.OffsetLimitPageable
import com.cogai.api.dashboard.entity.Camera
import org.springframework.data.domain.Page
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface CameraRepository : MongoRepository<Camera, String> {

    fun findOneById(id: String): Camera

    fun findAllByZoneId(zoneId: String): List<Camera>

    fun deleteCameraById(id: String)

    fun findAllByName(name: String, offsetLimitPageable: OffsetLimitPageable): Page<Camera>
}
