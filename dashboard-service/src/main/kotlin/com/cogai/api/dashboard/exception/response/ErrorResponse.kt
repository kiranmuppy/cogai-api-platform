package com.cogai.api.dashboard.exception.response

data class ErrorResponse(
    var message: String?,
    var code: Int
)
