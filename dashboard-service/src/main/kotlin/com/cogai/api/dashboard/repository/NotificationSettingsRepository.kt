package com.cogai.api.dashboard.repository

import com.cogai.api.dashboard.entity.NotificationSettings
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface NotificationSettingsRepository : MongoRepository<NotificationSettings, String> {

    fun findOneById(id: String): NotificationSettings

    fun deleteNotificationSettingsById(id: String)
}
