package com.cogai.api.dashboard.repository

import com.cogai.api.dashboard.dto.RawStatsResponse
import com.cogai.api.dashboard.entity.RawStats
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface AnalyticsRepository : MongoRepository<RawStats, String> {

    @Query("{fromTime : {\$gte : ?0}}")
    fun findAllByFromTimeGreaterThanEqualOrderByFromTime(fromTime: String): List<RawStats>

    @Query("{fromTime : {\$gte : ?0}, zoneId : ?1}")
    fun findAllByFromTimeGreaterThanEqualAndZoneIdOrderByFromTime(fromTime: String, zoneId: String): List<RawStats>

    @org.springframework.data.mongodb.repository.Aggregation(
        pipeline = [
            "{ \$match: {from_time: {\$gte : ?0, \$lte : ?1}} },",
            "{ \$group: { _id: { interval_start: { \$toDate: { \$subtract: [ { \$toLong: '\$from_time' }, { \$mod: [ { \$toLong: '\$from_time' }, ?2] } ] } } }," +
                "violations: {\$sum: '\$stats.violations'}, people: {\$sum: '\$stats.people'}, totalPeople: {\$sum: '\$stats.total_people'}, totalViolations: {\$sum: '\$stats.total_violations'}, video_link: {\$addToSet: '\$video_link'}" + "} },",
            "{ \$project: { _id: 0, stats: {violations: '\$violations', total_violations: '\$totalViolations', people: '\$people', total_people: '\$totalPeople' }, video_link: { \$reduce: { input: '\$video_link', initialValue: '', in: { \$concat: ['\$\$value', ',', '\$\$this']}} }, from_time: '\$_id.interval_start', to_time: { \$add: ['\$_id.interval_start', ?2]}}},",
            "{ \$sort: {from_time: 1} }"
        ]
    )
    fun findPreviousStatsAggregateWithOutZoneId(fromTime: LocalDateTime, toTime: LocalDateTime, interval: Int): List<RawStatsResponse>

    @org.springframework.data.mongodb.repository.Aggregation(
        pipeline = [
            "{ \$match: {from_time: {\$gte : ?0, \$lte : ?1},zone_id: ?3} },",
            "{ \$group: { _id: { zone_id: '\$zone_id', interval_start: { \$toDate: { \$subtract: [ { \$toLong: '\$from_time' }, { \$mod: [ { \$toLong: '\$from_time' }, ?2] } ] } } }," +
                "violations: {\$sum: '\$stats.violations'}, people: {\$sum: '\$stats.people'}, totalPeople: {\$sum: '\$stats.total_people'}, totalViolations: {\$sum: '\$stats.total_violations'}, video_link: {\$addToSet: '\$video_link'}" + "} },",
            "{ \$project: { _id: 0, zone_id: '\$_id.zone_id',stats: {violations: '\$violations', total_violations: '\$totalViolations', people: '\$people', total_people: '\$totalPeople' }, video_link: { \$reduce: { input: '\$video_link', initialValue: '', in: { \$concat: ['\$\$value', ',', '\$\$this']}} }, from_time: '\$_id.interval_start', to_time: { \$add: ['\$_id.interval_start', ?2]}}},",
            "{ \$sort: {from_time: 1} }"
        ]
    )
    fun findPreviousStatsAggregateSpecificZone(fromTime: LocalDateTime, toTime: LocalDateTime, interval: Int, zoneId: String): List<RawStatsResponse>
}
