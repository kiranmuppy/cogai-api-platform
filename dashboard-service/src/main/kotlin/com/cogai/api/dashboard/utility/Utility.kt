@file:JvmName("Utility")

package com.cogai.api.dashboard.utility

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Instant
import java.time.Instant.ofEpochMilli
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset.UTC
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.Calendar

fun Any.logger(): Logger = LoggerFactory.getLogger(this.javaClass)

fun LocalDateTime.formattedDate(): String {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
    return this.format(formatter)
}

fun Long.toLocalDateTime(): LocalDateTime {
    return LocalDateTime.ofInstant(ofEpochMilli(this), UTC)
}

fun String.formatToSpecificZone(timeZone: String): LocalDateTime {
    val instance: Instant = Instant.parse(this)
    val timeZone = Calendar.getInstance().timeZone
    return LocalDateTime.ofInstant(instance, ZoneId.of(timeZone.id)).truncatedTo(ChronoUnit.MILLIS)
}

fun String.toLocalDateTime(timeZone: String): LocalDateTime {
    val instance: Instant = Instant.parse(this)
    return LocalDateTime.ofInstant(instance, UTC)
}

fun LocalDateTime.toUtCString(): String {
    return this.atZone(UTC).format(
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    )
}

fun LocalDateTime.toLocalDateTimeAtSpecificZone(timeZone: String): ZonedDateTime {
    return this.atZone(ZoneId.of(timeZone))
}

fun String.toBrowserSpecific(): LocalDateTime {
    return LocalDateTime.now().minusMinutes(30).truncatedTo(ChronoUnit.MILLIS)
}
