package com.cogai.api.dashboard.controller

import com.cogai.api.dashboard.dto.UserRequestBody
import com.cogai.api.dashboard.dto.UserResponseBody
import com.cogai.api.dashboard.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.NotBlank

@RestController
@CrossOrigin(origins = ["*"])
class UserController(
    private val userService: UserService
) {

    @PostMapping("user")
    fun createUser(@RequestBody requestBody: UserRequestBody): ResponseEntity<UserResponseBody> {
        return ResponseEntity(
            userService.createUser(requestBody),
            HttpStatus.CREATED
        )
    }

    @DeleteMapping("user/{id}")
    fun deleteUser(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<*> {
        userService.deleteUser(id)
        return ResponseEntity<Any>(HttpStatus.NO_CONTENT)
    }

    @GetMapping("user/{id}")
    fun getUser(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<UserResponseBody> {
        return ResponseEntity(
            userService.getUser(id),
            HttpStatus.OK
        )
    }

    @PutMapping("user/{id}")
    fun updateUser(
        @NotBlank @PathVariable id: String,
        @RequestBody requestBody: UserRequestBody
    ): ResponseEntity<*> {
        return ResponseEntity(
            userService.updateUser(id, requestBody),
            HttpStatus.OK
        )
    }

    @GetMapping("user")
    fun getUserList(
        @RequestParam(defaultValue = "0") offset: Int,
        @RequestParam(defaultValue = "10") limit: Int,
        @RequestParam(defaultValue = "") name: String
    ): ResponseEntity<*> {
        return ResponseEntity(
            userService.getListOfUsers(name, offset, limit),
            HttpStatus.OK
        )
    }
}
