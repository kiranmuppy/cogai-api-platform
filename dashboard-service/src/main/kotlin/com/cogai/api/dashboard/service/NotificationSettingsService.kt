package com.cogai.api.dashboard.service

import com.cogai.api.dashboard.configuration.OffsetLimitPageable
import com.cogai.api.dashboard.dto.NotificationSettingsRequestBody
import com.cogai.api.dashboard.dto.NotificationSettingsResponseBody
import com.cogai.api.dashboard.entity.Camera
import com.cogai.api.dashboard.entity.Filters
import com.cogai.api.dashboard.entity.NotificationSettings
import com.cogai.api.dashboard.exception.ResourceNotFoundException
import com.cogai.api.dashboard.repository.CameraRepository
import com.cogai.api.dashboard.repository.NotificationSettingsRepository
import com.cogai.api.dashboard.utility.formattedDate
import com.cogai.api.dashboard.utility.logger
import com.cogai.api.dashboard.utility.toNotificationSettingsEntity
import com.cogai.api.dashboard.utility.toNotificationSettingsResponseBody
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class NotificationSettingsService(
    private val notificationSettingsRepository: NotificationSettingsRepository,
    private val cameraRepository: CameraRepository
) {

    private val log = logger()

    fun createNotificationSettings(requestBody: NotificationSettingsRequestBody): NotificationSettingsResponseBody {
        try {
            val camerasEnabled = requestBody.selectedCameras.enableNotificationsForCamera()
            return notificationSettingsRepository.save(
                requestBody.toNotificationSettingsEntity(camerasEnabled)
            ).toNotificationSettingsResponseBody()
        } catch (ex: ResourceNotFoundException) {
            log.error(ex.message, ex)
            throw ResourceNotFoundException("No resource found with the mentioned Id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    fun getNotificationSettings(id: String): NotificationSettingsResponseBody {
        return try {
            notificationSettingsRepository.findOneById(id).toNotificationSettingsResponseBody()
        } catch (e: Exception) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        }
    }

    fun deleteNotificationSettings(id: String) {
        return try {
            notificationSettingsRepository.deleteNotificationSettingsById(id)
        } catch (e: Exception) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        }
    }

    fun getListOfNotificationSettings(offset: Int, limit: Int): Page<NotificationSettingsResponseBody> {
        return notificationSettingsRepository.findAll(OffsetLimitPageable(offset, limit, Sort.unsorted())).map { it.toNotificationSettingsResponseBody() }
    }

    fun updateNotificationSettings(id: String, requestBody: NotificationSettingsRequestBody): NotificationSettingsResponseBody {
        try {
            val camerasEnabled = requestBody.selectedCameras.enableNotificationsForCamera()
            val entity = notificationSettingsRepository.findOneById(id)
            val entityToBeUpdated = NotificationSettings(
                action = requestBody.action,
                filters = Filters(
                    requestBody.filters.minNoOfViolations,
                    requestBody.filters.maxNoOfViolations
                ),
                userId = requestBody.userId,
                zoneId = requestBody.zoneId,
                cameras = camerasEnabled,
                createdBy = entity.createdBy,
                modifiedBy = requestBody.modifiedBy,
                modifiedAt = LocalDateTime.now().formattedDate()
            )
            return notificationSettingsRepository.save(entityToBeUpdated).toNotificationSettingsResponseBody()
        } catch (ex: ResourceNotFoundException) {
            log.error(ex.message, ex)
            throw ResourceNotFoundException("No resource found with the mentioned Id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    private fun Map<String, Boolean>.enableNotificationsForCamera(): List<Camera> {
        val camerasEnabled = mutableListOf<Camera>()
        this.forEach { (key, value) ->
            try {
                val camera = cameraRepository.findOneById(key).copy(notificationSettingEnabled = value)
                camerasEnabled.add(camera)
            } catch (ex: ResourceNotFoundException) {
                log.error("No resource found with the mentioned $key")
            }
        }
        return camerasEnabled
    }
}
