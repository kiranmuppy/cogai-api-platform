package com.cogai.api.dashboard.service

import com.cogai.api.dashboard.configuration.OffsetLimitPageable
import com.cogai.api.dashboard.dto.ZoneRequestBody
import com.cogai.api.dashboard.dto.ZoneResponseBody
import com.cogai.api.dashboard.entity.Zone
import com.cogai.api.dashboard.exception.ResourceNotFoundException
import com.cogai.api.dashboard.repository.ZoneRepository
import com.cogai.api.dashboard.utility.formattedDate
import com.cogai.api.dashboard.utility.logger
import com.cogai.api.dashboard.utility.toZoneEntity
import com.cogai.api.dashboard.utility.toZoneResponseBody
import com.cogai.api.dashboard.utility.toZoneSettings
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class ZoneService(
    private val zoneRepository: ZoneRepository
) {

    private val log = logger()

    fun createZone(requestBody: ZoneRequestBody): ZoneResponseBody {
        val zone = zoneRepository.save(requestBody.toZoneEntity())
        return zone.toZoneResponseBody()
    }

    fun getZone(id: String): ZoneResponseBody {
        return try {
            zoneRepository.findOneById(id).toZoneResponseBody()
        } catch (e: ResourceNotFoundException) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    fun updateZone(id: String, requestBody: ZoneRequestBody): ZoneResponseBody {
        return try {
            val zoneEntity = zoneRepository.findOneById(id)
            val updatedZoneEntity = Zone(
                id = zoneEntity.id,
                name = requestBody.name,
                settings = requestBody.settings.toZoneSettings(),
                createdBy = zoneEntity.createdBy,
                modifiedBy = requestBody.modifiedBy,
                createdAt = zoneEntity.createdAt,
                modifiedAt = LocalDateTime.now().formattedDate()
            )
            zoneRepository.save(updatedZoneEntity).toZoneResponseBody()
        } catch (e: Exception) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        }
    }

    fun deleteZone(id: String) {
        return try {
            zoneRepository.deleteZoneById(id)
        } catch (e: ResourceNotFoundException) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    fun getListOfZones(name: String, offset: Int, limit: Int): Page<ZoneResponseBody> {
        return if (name.isEmpty()) {
            zoneRepository.findAll(OffsetLimitPageable(offset, limit, Sort.unsorted())).map { it.toZoneResponseBody() }
        } else {
            zoneRepository.findAllByName(name, OffsetLimitPageable(offset, limit, Sort.unsorted())).map { it.toZoneResponseBody() }
        }
    }
}
