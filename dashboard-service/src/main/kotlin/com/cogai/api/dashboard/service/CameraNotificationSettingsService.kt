package com.cogai.api.dashboard.service

import com.cogai.api.dashboard.configuration.OffsetLimitPageable
import com.cogai.api.dashboard.dto.CameraNotificationSettingsRequestBody
import com.cogai.api.dashboard.dto.CameraNotificationSettingsResponseBody
import com.cogai.api.dashboard.dto.UpdateCameraNotificationSettingsRequestBody
import com.cogai.api.dashboard.entity.CameraNotificationSettings
import com.cogai.api.dashboard.exception.ResourceNotFoundException
import com.cogai.api.dashboard.repository.CameraNotificationSettingsRepository
import com.cogai.api.dashboard.utility.formattedDate
import com.cogai.api.dashboard.utility.logger
import com.cogai.api.dashboard.utility.toCameraNotificationSettingsEntity
import com.cogai.api.dashboard.utility.toCameraNotificationSettingsResponseBody
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class CameraNotificationSettingsService(
    private val cameraNotificationSettingsRepository: CameraNotificationSettingsRepository
) {

    private val log = logger()

    fun createCameraNotificationSettings(requestBody: CameraNotificationSettingsRequestBody): CameraNotificationSettingsResponseBody {
        return cameraNotificationSettingsRepository.save(
            requestBody.toCameraNotificationSettingsEntity()
        ).toCameraNotificationSettingsResponseBody()
    }

    fun getCameraNotificationSettingsById(id: String): CameraNotificationSettingsResponseBody {
        return try {
            cameraNotificationSettingsRepository.findOneById(id).toCameraNotificationSettingsResponseBody()
        } catch (e: ResourceNotFoundException) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    fun deleteCameraNotificationSettings(id: String) {
        return try {
            cameraNotificationSettingsRepository.deleteCameraNotificationSettingsById(id)
        } catch (e: ResourceNotFoundException) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    fun getListOfCameraNotificationSettings(offset: Int, limit: Int): Page<CameraNotificationSettingsResponseBody> {
        return cameraNotificationSettingsRepository.findAll(OffsetLimitPageable(offset, limit, Sort.unsorted())).map { it.toCameraNotificationSettingsResponseBody() }
    }

    fun updateCameraNotificationSettings(id: String, requestBody: UpdateCameraNotificationSettingsRequestBody): CameraNotificationSettingsResponseBody {
        try {
            val entity = cameraNotificationSettingsRepository.findOneById(id)
            val entityToBeUpdated = CameraNotificationSettings(
                id = id,
                zoneId = entity.zoneId,
                minNumOfViolations = requestBody.filters.minNoOfViolations,
                maxNumOfViolations = requestBody.filters.maxNoOfViolations,
                cameraId = entity.cameraId,
                action = requestBody.action,
                userId = requestBody.userId,
                createdBy = entity.createdBy,
                modifiedBy = requestBody.modifiedBy,
                createdAt = entity.createdAt,
                modifiedAt = LocalDateTime.now().formattedDate()
            )
            return cameraNotificationSettingsRepository.save(entityToBeUpdated).toCameraNotificationSettingsResponseBody()
        } catch (ex: ResourceNotFoundException) {
            log.error(ex.message, ex)
            throw ResourceNotFoundException("No resource found with the mentioned Id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    fun getNotificationSettingsByCamera(cameraId: String): List<CameraNotificationSettingsResponseBody> {
        try {
            return cameraNotificationSettingsRepository.findAllByCameraId(cameraId).map { it.toCameraNotificationSettingsResponseBody() }
        } catch (ex: ResourceNotFoundException) {
            log.error(ex.message, ex)
            throw ResourceNotFoundException("No resource found with the mentioned Id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }
}
