package com.cogai.api.dashboard.service

import com.cogai.api.dashboard.entity.Notifications
import com.cogai.api.dashboard.repository.NotificationsRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.stereotype.Service

@Service
class NotificationsService(
    private val notificationsRepository: NotificationsRepository
) {
    
    fun getListOfNotifications(offset: Int, limit: Int): Page<Notifications> {
        return PageImpl(notificationsRepository.findAll().reversed().subList(0, 19))
    }
}
