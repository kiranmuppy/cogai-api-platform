package com.cogai.api.dashboard.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("mongo")
data class MongoProperties(
    val url: String
)

data class Test(
    val test: String
)
