package com.cogai.api.dashboard.controller

import com.cogai.api.dashboard.dto.ZoneRequestBody
import com.cogai.api.dashboard.dto.ZoneResponseBody
import com.cogai.api.dashboard.service.ZoneService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.NotBlank

@RestController
@CrossOrigin(origins = ["*"])
class ZoneController(
    private val zoneService: ZoneService
) {

    @PostMapping("zone")
    fun createZone(@RequestBody requestBody: ZoneRequestBody): ResponseEntity<ZoneResponseBody> {
        return ResponseEntity(
            zoneService.createZone(requestBody),
            HttpStatus.CREATED
        )
    }

    @DeleteMapping("zone/{id}")
    fun deleteZone(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<*> {
        zoneService.deleteZone(id)
        return ResponseEntity<Any>(HttpStatus.NO_CONTENT)
    }

    @GetMapping("zone/{id}")
    fun getZone(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<*> {
        return ResponseEntity(
            zoneService.getZone(id),
            HttpStatus.OK
        )
    }

    @PutMapping("zone/{id}")
    fun updateZone(
        @NotBlank @PathVariable id: String,
        @RequestBody requestBody: ZoneRequestBody
    ): ResponseEntity<ZoneResponseBody> {
        return ResponseEntity(
            zoneService.updateZone(id, requestBody),
            HttpStatus.OK
        )
    }

    @GetMapping("zone")
    fun getListOfZones(
        @RequestParam(defaultValue = "0") offset: Int,
        @RequestParam(defaultValue = "10") limit: Int,
        @RequestParam(defaultValue = "") name: String
    ): ResponseEntity<*> {
        return ResponseEntity(
            zoneService.getListOfZones(name, offset, limit),
            HttpStatus.OK
        )
    }
}
