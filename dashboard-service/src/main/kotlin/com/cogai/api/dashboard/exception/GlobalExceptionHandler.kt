package com.cogai.api.dashboard.exception

import com.cogai.api.dashboard.exception.response.ErrorResponse
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.ResponseEntity
import org.springframework.web.HttpRequestMethodNotSupportedException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class GlobalExceptionHandler :
    ResponseEntityExceptionHandler() {
    @ExceptionHandler(ResourceNotFoundException::class)
    fun resourceNotFoundException(
        ex: ResourceNotFoundException,
        request: WebRequest
    ): ResponseEntity<*> {
        return ResponseEntity<Any>(
            ErrorResponse("No resource found with the mentioned Id", NOT_FOUND.value()),
            HttpStatus.NOT_FOUND
        )
    }

    @ExceptionHandler(NumberFormatException::class)
    fun numberFormatException(
        ex: NumberFormatException,
        request: WebRequest
    ): ResponseEntity<*> {
        return ResponseEntity<Any>(
            ErrorResponse("Unable to Format the given input to Number", HttpStatus.BAD_REQUEST.value()),
            HttpStatus.BAD_REQUEST
        )
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException::class)
    fun methodArgumentTypeMismatchException(
        ex: MethodArgumentTypeMismatchException,
        request: WebRequest
    ): ResponseEntity<*> {
        return ResponseEntity<Any>(
            ErrorResponse("Invalid parameter Passed as Id in input", HttpStatus.BAD_REQUEST.value()),
            HttpStatus.BAD_REQUEST
        )
    }

    override
    fun handleHttpRequestMethodNotSupported(
        ex: HttpRequestMethodNotSupportedException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        return ResponseEntity(
            ErrorResponse("No Method is supported for this Request", HttpStatus.METHOD_NOT_ALLOWED.value()),
            HttpStatus.METHOD_NOT_ALLOWED
        )
    }
}
