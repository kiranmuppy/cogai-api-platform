package com.cogai.api.dashboard.configuration

import com.cogai.api.dashboard.authorization.jwt.JwtAuthenticationEntryPoint
import com.cogai.api.dashboard.authorization.jwt.JwtRequestFilter
import com.cogai.api.dashboard.authorization.jwt.JwtUserDetailsService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@Configuration
@EnableWebSecurity
class SecurityConfiguration(
    private val jwtUserDetailsService: JwtUserDetailsService,
    private val bCryptPasswordEncoder: PasswordEncoder,
    private val jwtRequestFilter: JwtRequestFilter,
    private val jwtAuthenticationEntryPoint: JwtAuthenticationEntryPoint
) : WebSecurityConfigurerAdapter() {

    override fun configure(webSecurity: WebSecurity) {
        webSecurity.ignoring().antMatchers(
            "/health/check", "/", "/v2/api-docs", "/configuration/ui",
            "/swagger-resources/**", "/configuration/**", "/favicon.ico", "/swagger-ui/**", "/webjars/**"
        )
    }

    @Throws(Exception::class)
    override fun configure(httpSecurity: HttpSecurity) {
        httpSecurity
            .cors()
            .and()
            .csrf().disable()
            .headers().frameOptions().deny()
            .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter::class.java)
            .authorizeRequests()
            .antMatchers("/authenticate").permitAll()
            .antMatchers("/camera/**").hasAuthority("Admin")
            .antMatchers("/user/**").hasAuthority("Admin")
            .antMatchers("/notification-settings/**").hasAuthority("Admin")
            .antMatchers("/camera-notification-settings/**").hasAuthority("Admin")
            .antMatchers("/zone/**").hasAuthority("Admin")
            .antMatchers("/analytics/**").hasAnyAuthority("Admin", "Security Personnel", "Paramedics", "Plant Manager")
            .anyRequest().authenticated().and().exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
    }

    @Throws(Exception::class)
    override fun configure(builder: AuthenticationManagerBuilder) {
        builder.userDetailsService(jwtUserDetailsService).passwordEncoder(bCryptPasswordEncoder)
    }

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager? {
        return super.authenticationManagerBean()
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.allowedOrigins = listOf("*")
        configuration.allowedHeaders = listOf("*")
        configuration.allowCredentials = true
        configuration.allowedMethods = listOf("*")
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }
}
