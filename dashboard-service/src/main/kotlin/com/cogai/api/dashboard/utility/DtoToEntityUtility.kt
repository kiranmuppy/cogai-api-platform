package com.cogai.api.dashboard.utility

import com.cogai.api.dashboard.dto.CameraNotificationSettingsRequestBody
import com.cogai.api.dashboard.dto.CameraRequestBody
import com.cogai.api.dashboard.dto.NotificationSettingsRequestBody
import com.cogai.api.dashboard.dto.UserRequestBody
import com.cogai.api.dashboard.dto.ZoneRequestBody
import com.cogai.api.dashboard.dto.ZoneSettingsRequestBody
import com.cogai.api.dashboard.entity.Camera
import com.cogai.api.dashboard.entity.CameraNotificationSettings
import com.cogai.api.dashboard.entity.Filters
import com.cogai.api.dashboard.entity.NotificationSettings
import com.cogai.api.dashboard.entity.User
import com.cogai.api.dashboard.entity.Zone
import com.cogai.api.dashboard.entity.ZoneSettings

fun ZoneRequestBody.toZoneEntity() = Zone(
    name = this.name,
    settings = this.settings.toZoneSettings(),
    createdBy = this.createdBy,
    modifiedBy = this.modifiedBy
)

fun ZoneSettingsRequestBody.toZoneSettings() = ZoneSettings(
    violationDistance = this.violationDistance,
    violationDuration = this.violationDuration
)

fun CameraRequestBody.toCameraEntity() = Camera(
    name = this.name,
    zoneId = this.zoneId,
    link = this.link,
    rawLink = this.rawLink,
    resolution = this.resolution,
    type = this.type,
    createdBy = this.createdBy,
    modifiedBy = this.modifiedBy
)

fun UserRequestBody.toUserEntity(password: String) = User(
    email = this.email,
    firstName = this.firstName,
    lastName = this.lastName,
    userName = this.userName,
    persona = this.persona,
    mobileNumber = this.mobileNumber,
    userProfileImage = this.userProfileImage,
    password = password,
    createdBy = this.createdBy,
    modifiedBy = this.modifiedBy
)

fun NotificationSettingsRequestBody.toNotificationSettingsEntity
        (cameras: List<Camera>) = NotificationSettings(
    userId = this.userId,
    filters = Filters(
        minNumOfViolations = this.filters.minNoOfViolations,
        maxNumOfViolations = this.filters.maxNoOfViolations
    ),
    action = this.action,
    zoneId = this.zoneId,
    cameras = cameras,
    createdBy = this.createdBy,
    modifiedBy = this.modifiedBy
)

fun CameraNotificationSettingsRequestBody.toCameraNotificationSettingsEntity() = CameraNotificationSettings(
    zoneId = this.zoneId,
    cameraId = this.cameraId,
    minNumOfViolations = this.filters.minNoOfViolations,
    maxNumOfViolations = this.filters.maxNoOfViolations,
    action = this.action,
    userId = this.userId,
    createdBy = this.createdBy,
    modifiedBy = this.modifiedBy
)
