package com.cogai.api.dashboard.entity

import com.cogai.api.dashboard.utility.formattedDate
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.IndexDirection
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.math.BigInteger
import java.time.LocalDateTime

@Document(collection = "camera")
data class Camera(
    @Id
    val id: String = ObjectId.get().toHexString(),
    val name: String,
    val zoneId: String,
    val link: String,
    val rawLink: String,
    val resolution: String,
    val type: String,
    val notificationSettingEnabled: Boolean = false,
    val createdBy: String,
    val modifiedBy: String,
    val createdAt: String = LocalDateTime.now().formattedDate(),
    val modifiedAt: String = LocalDateTime.now().formattedDate()
)

@Document(collection = "zone")
data class Zone(
    @Id
    val id: String = ObjectId.get().toHexString(),
    val name: String,
    val settings: ZoneSettings,
    val createdBy: String,
    val modifiedBy: String,
    val createdAt: String = LocalDateTime.now().formattedDate(),
    val modifiedAt: String = LocalDateTime.now().formattedDate()
)

data class ZoneSettings(
    val violationDistance: String,
    val violationDuration: String,
    val motion: Int = 0,
    val initializeIn: Int = 0
)

@Document(collection = "user")
data class User(
    @Id
    val id: String = ObjectId.get().toHexString(),
    @Indexed(unique = true, direction = IndexDirection.DESCENDING, dropDups = true)
    val email: String,
    val firstName: String,
    val lastName: String,
    val userName: String,
    val persona: String,
    val mobileNumber: String,
    val userProfileImage: String,
    val password: String,
    val createdBy: String,
    val modifiedBy: String,
    val createdAt: String = LocalDateTime.now().formattedDate(),
    val modifiedAt: String = LocalDateTime.now().formattedDate()
)

@Document(collection = "notification_settings")
data class NotificationSettings(
    @Id
    val id: String = ObjectId.get().toHexString(),
    val userId: String,
    val filters: Filters,
    val action: String,
    val zoneId: String,
    val cameras: List<Camera>,
    val createdBy: String,
    val modifiedBy: String,
    val createdAt: String = LocalDateTime.now().formattedDate(),
    val modifiedAt: String = LocalDateTime.now().formattedDate()
)

data class Filters(
    val minNumOfViolations: Int,
    val maxNumOfViolations: Int
)

@Document(collection = "rawstats")
data class RawStats(
    @Id
    val id: ObjectId = ObjectId.get(),
    @Field("zone_id")
    val zoneId: String,
    @Field("camera_id")
    val cameraId: String,
    @Field("stats")
    val stats: Stats,
    @Field("video_link")
    val videoLink: String,
    @Field("from_time")
    val fromTime: LocalDateTime,
    @Field("to_time")
    val toTime: LocalDateTime,
    @Field("created_at")
    val createdAt: LocalDateTime,
    @Field("modified_at")
    val modifiedAt: LocalDateTime
)

data class Stats(
    @Field("violations")
    val violations: BigInteger,
    @Field("total_violations")
    val totalViolations: BigInteger,
    @Field("people")
    val people: BigInteger,
    @Field("total_people")
    val totalPeople: BigInteger
)

@Document(collection = "notifications")
data class Notifications(
    @Id
    val id: String = ObjectId.get().toHexString(),
    @Field("notification_id")
    val notificationId: String,
    @Field("violation_count")
    val violationCount: Int,
    @Field("zone_id")
    val zoneId: String,
    @Field("camera_id")
    val cameraId: String,
    @Field("notification_settings_id")
    val notificationSettingsId: String,
    @Field("notification_action")
    val notificationAction: String,
    @Field("violation_occurred_at")
    val violationOccurredAt: LocalDateTime,
    @Field("notification_sent_at")
    val notificationSentAt: LocalDateTime,
    @Field("notification_response")
    val notificationResponseEntity: NotificationResponseEntity
)

data class NotificationResponseEntity(
    @Field("message_id")
    val messageId: String,
    @Field("message_status")
    val messageStatus: String
)

@Document(collection = "camera_notification_settings")
data class CameraNotificationSettings(
    @Id
    val id: String = ObjectId.get().toHexString(),
    val zoneId: String,
    val cameraId: String,
    val minNumOfViolations: Int,
    val maxNumOfViolations: Int,
    val action: String,
    val userId: String,
    val createdBy: String,
    val modifiedBy: String,
    val createdAt: String = LocalDateTime.now().formattedDate(),
    val modifiedAt: String = LocalDateTime.now().formattedDate()
)
