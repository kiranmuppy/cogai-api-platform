package com.cogai.api.dashboard.repository

import com.cogai.api.dashboard.entity.CameraNotificationSettings
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface CameraNotificationSettingsRepository : MongoRepository<CameraNotificationSettings, String> {

    fun findOneById(id: String): CameraNotificationSettings

    fun deleteCameraNotificationSettingsById(id: String)

    fun findAllByCameraId(cameraId: String): List<CameraNotificationSettings>
}
