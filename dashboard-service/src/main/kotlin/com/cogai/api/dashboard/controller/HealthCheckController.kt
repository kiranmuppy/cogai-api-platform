package com.cogai.api.dashboard.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HealthCheckController {
    @GetMapping("health/check")
    fun checkAppStatus(): ResponseEntity<String> {
        return ResponseEntity("Healthy from Dashboard Service.", HttpStatus.OK)
    }
    @GetMapping("/")
    fun index(): ResponseEntity<String> {
        return ResponseEntity("", HttpStatus.NOT_FOUND)
    }
}
