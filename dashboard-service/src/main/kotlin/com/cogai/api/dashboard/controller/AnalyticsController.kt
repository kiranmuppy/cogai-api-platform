package com.cogai.api.dashboard.controller

import com.cogai.api.dashboard.dto.LatestAnalyticsRequestBody
import com.cogai.api.dashboard.dto.LatestRawStatsResponse
import com.cogai.api.dashboard.dto.PreviousAnalyticsRequestBody
import com.cogai.api.dashboard.dto.RawStatsResponse
import com.cogai.api.dashboard.dto.StatsResponse
import com.cogai.api.dashboard.entity.RawStats
import com.cogai.api.dashboard.service.AnalyticsService
import com.cogai.api.dashboard.utility.formatToSpecificZone
import com.cogai.api.dashboard.utility.toBrowserSpecific
import com.cogai.api.dashboard.utility.toLocalDateTimeAtSpecificZone
import org.springframework.cache.annotation.Cacheable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin(origins = ["*"])
class AnalyticsController(
    private val analyticsService: AnalyticsService
) {
    @PostMapping("analytics")
    fun getLatestStats(
        @RequestBody requestBody: LatestAnalyticsRequestBody
    ): ResponseEntity<*> {
        val stats = analyticsService.getLatestStats(requestBody.time.toBrowserSpecific(), requestBody.zoneId)
        return ResponseEntity(
            stats.map { it.toRawStatsResponse(requestBody.timeZone) },
            HttpStatus.OK
        )
    }

    @GetMapping("analytics-interval")
    fun getLatestStatsForInterval(
        @RequestParam(required = false) zoneId: String?
    ): ResponseEntity<*> {
        return ResponseEntity(
            analyticsService.getLatestStatsByInterval(zoneId),
            HttpStatus.OK
        )
    }

    @GetMapping("analytics-net-today")
    fun getNetStatsForToday(
        @RequestParam(required = false) zoneId: String?
    ): ResponseEntity<*> {
        return ResponseEntity(
            analyticsService.getNetStatsForToday(zoneId),
            HttpStatus.OK
        )
    }

    @Cacheable("previousStats")
    @PostMapping("analytics/previous")
    fun getPreviousStats(
        @RequestBody requestBody: PreviousAnalyticsRequestBody
    ): ResponseEntity<*> {
        val previousStats = analyticsService.getPreviousStatsAggregate(
            requestBody.fromTime.formatToSpecificZone(requestBody.timeZone),
            requestBody.toTime.formatToSpecificZone(requestBody.timeZone),
            requestBody.interval, requestBody.zoneId
        )
        return ResponseEntity(
            previousStats.map { it.toPreviousRawStatsResponse(requestBody.timeZone) },
            HttpStatus.OK
        )
    }

    private fun RawStats.toRawStatsResponse(timeZone: String): LatestRawStatsResponse = LatestRawStatsResponse(
        zone_id = this.zoneId,
        stats = StatsResponse(
            violations = this.stats.violations,
            total_violations = this.stats.totalViolations,
            people = this.stats.people,
            total_people = this.stats.totalPeople
        ),
        video_link = this.videoLink,
        from_time = this.fromTime.toLocalDateTimeAtSpecificZone(timeZone),
        to_time = this.toTime.toLocalDateTimeAtSpecificZone(timeZone)
    )

    private fun RawStatsResponse.toPreviousRawStatsResponse(timeZone: String): LatestRawStatsResponse = LatestRawStatsResponse(
        zone_id = this.zone_id,
        stats = StatsResponse(
            violations = this.stats.violations,
            total_violations = this.stats.total_violations,
            people = this.stats.people,
            total_people = this.stats.total_people
        ),
        video_link = this.video_link.getActiveVideoLink(),
        from_time = this.from_time.toLocalDateTimeAtSpecificZone(timeZone),
        to_time = this.to_time.toLocalDateTimeAtSpecificZone(timeZone)
    )

    private fun String.getActiveVideoLink(): String {
        return this.removePrefix(",").split(",").toList()[0]
    }
}
