package com.cogai.api.dashboard.service

import com.cogai.api.dashboard.configuration.OffsetLimitPageable
import com.cogai.api.dashboard.dto.UserRequestBody
import com.cogai.api.dashboard.dto.UserResponseBody
import com.cogai.api.dashboard.entity.User
import com.cogai.api.dashboard.exception.ResourceNotFoundException
import com.cogai.api.dashboard.repository.UserRepository
import com.cogai.api.dashboard.utility.formattedDate
import com.cogai.api.dashboard.utility.logger
import com.cogai.api.dashboard.utility.toUserEntity
import com.cogai.api.dashboard.utility.toUserResponseBody
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class UserService(
    private val userRepository: UserRepository,
    private val bCryptPasswordEncoder: PasswordEncoder
) : UserDetailsService {

    private val log = logger()

    fun createUser(requestBody: UserRequestBody): UserResponseBody {
        return userRepository.save(requestBody.toUserEntity(bCryptPasswordEncoder.encode(requestBody.password))).toUserResponseBody()
    }

    fun getUser(id: String): UserResponseBody {
        return try {
            userRepository.findOneById(id).toUserResponseBody()
        } catch (e: ResourceNotFoundException) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    fun deleteUser(id: String) {
        return try {
            userRepository.deleteUserById(id)
        } catch (e: ResourceNotFoundException) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        } catch (ex: Exception) {
            log.error(ex.message, ex)
            throw Exception("Un expected server error")
        }
    }

    fun getListOfUsers(name: String, offset: Int, limit: Int): Page<UserResponseBody> {
        return if (name.isEmpty()) {
            userRepository.findAll(OffsetLimitPageable(offset, limit, Sort.unsorted())).map { it.toUserResponseBody() }
        } else {
            userRepository.findAllByFirstNameAndLastName(name, OffsetLimitPageable(offset, limit, Sort.unsorted())).map { it.toUserResponseBody() }
        }
    }

    fun updateUser(id: String, requestBody: UserRequestBody): UserResponseBody {
        return try {
            val userEntity = userRepository.findOneById(id)
            val userEntityToBeUpdated = User(
                id = userEntity.id,
                email = requestBody.email,
                firstName = requestBody.firstName,
                lastName = requestBody.lastName,
                userName = requestBody.userName,
                persona = requestBody.persona,
                mobileNumber = requestBody.mobileNumber,
                userProfileImage = requestBody.userProfileImage,
                password = bCryptPasswordEncoder.encode(requestBody.password),
                createdBy = userEntity.createdBy,
                modifiedBy = requestBody.modifiedBy,
                createdAt = userEntity.createdAt,
                modifiedAt = LocalDateTime.now().formattedDate()
            )
            userRepository.save(userEntityToBeUpdated).toUserResponseBody()
        } catch (e: Exception) {
            log.error(e.message)
            throw ResourceNotFoundException("No resource found with the mentioned Id $id")
        }
    }

    override fun loadUserByUsername(username: String): UserDetails {
        TODO("Not yet implemented")
    }
}
