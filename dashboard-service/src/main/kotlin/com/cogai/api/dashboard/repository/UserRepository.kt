package com.cogai.api.dashboard.repository

import com.cogai.api.dashboard.configuration.OffsetLimitPageable
import com.cogai.api.dashboard.entity.User
import org.springframework.data.domain.Page
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : MongoRepository<User, String> {

    fun findByUserName(userName: String): User?

    fun findByEmail(email: String): User?

    fun deleteUserByEmail(email: String)

    fun deleteUserById(id: String)

    fun findOneById(id: String): User

    fun findAllByFirstNameAndLastName(name: String, offsetLimitPageable: OffsetLimitPageable): Page<User>
}
