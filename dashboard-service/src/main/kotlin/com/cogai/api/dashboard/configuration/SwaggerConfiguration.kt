package com.cogai.api.dashboard.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.builders.RequestParameterBuilder
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfiguration {

    @Bean
    fun api(): Docket {
        val requestParameterBuilder = RequestParameterBuilder()
            .name("Authorization")
            .description("JWT Token")
            .required(true)
            .`in`("header").build()
        return Docket(DocumentationType.SWAGGER_2)
            .apiInfo(
                ApiInfo(
                    "Dashboard service",
                    "All the end points for Dashboard service",
                    "0.0.1",
                    "All rights CogAi ©2020",
                    Contact(
                        "Cogai Back end Support Team",
                        "www.cogai.com",
                        "apisupport@cogai.com"
                    ),
                    "",
                    "",
                    emptyList()
                )
            )
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.cogai.api.dashboard.controller"))
            .paths(PathSelectors.any())
            .build()
            .globalRequestParameters(listOf(requestParameterBuilder))
    }
}
