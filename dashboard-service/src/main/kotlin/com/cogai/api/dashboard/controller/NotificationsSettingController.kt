package com.cogai.api.dashboard.controller

import com.cogai.api.dashboard.dto.NotificationSettingsRequestBody
import com.cogai.api.dashboard.dto.NotificationSettingsResponseBody
import com.cogai.api.dashboard.service.NotificationSettingsService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.NotBlank

@RestController
@CrossOrigin(origins = ["*"])
class NotificationsSettingController(
    private val notificationSettingsService: NotificationSettingsService
) {

    @PostMapping("notification-setting")
    fun createNotificationsSetting(@RequestBody requestBody: NotificationSettingsRequestBody): ResponseEntity<NotificationSettingsResponseBody> {
        return ResponseEntity(
            notificationSettingsService.createNotificationSettings(requestBody),
            HttpStatus.CREATED
        )
    }

    @DeleteMapping("notification-setting/{id}")
    fun deleteNotificationsSetting(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<*> {
        notificationSettingsService.deleteNotificationSettings(id)
        return ResponseEntity<Any>(HttpStatus.NO_CONTENT)
    }

    @GetMapping("notification-setting/{id}")
    fun getNotificationsSetting(
        @NotBlank @PathVariable id: String
    ): ResponseEntity<NotificationSettingsResponseBody> {
        return ResponseEntity(
            notificationSettingsService.getNotificationSettings(id),
            HttpStatus.OK
        )
    }

    @PutMapping("notification-setting/{id}")
    fun updateNotificationsSetting(
        @NotBlank @PathVariable id: String,
        @RequestBody requestBody: NotificationSettingsRequestBody
    ): ResponseEntity<*> {
        return ResponseEntity(
            notificationSettingsService.updateNotificationSettings(id, requestBody),
            HttpStatus.OK
        )
    }

    @GetMapping("notification-setting")
    fun x(
        @RequestParam(defaultValue = "0") offset: Int,
        @RequestParam(defaultValue = "10") limit: Int
    ): ResponseEntity<*> {
        return ResponseEntity(
            notificationSettingsService.getListOfNotificationSettings(offset, limit),
            HttpStatus.OK
        )
    }
}
