package com.cogai.api.dashboard.dto

import com.cogai.api.dashboard.entity.User
import java.math.BigInteger
import java.time.LocalDateTime
import java.time.ZonedDateTime

data class CameraResponseBody(
    val id: String,
    val name: String,
    val zoneId: String,
    val link: String,
    val rawLink: String,
    val resolution: String,
    val type: String,
    val notificationSettingEnabled: Boolean,
    val createdBy: String,
    val modifiedBy: String,
    val createdAt: String,
    val modifiedAt: String
)

data class ZoneResponseBody(
    val id: String,
    val name: String,
    val settings: ZoneSettingsResponseBody,
    val createdBy: String,
    val modifiedBy: String,
    val createdAt: String,
    val modifiedAt: String
)

data class ZoneSettingsResponseBody(
    val violationDistance: String,
    val violationDuration: String,
    val motion: Int = 0,
    val initializeIn: Int = 0
)

data class UserResponseBody(
    val id: String,
    val email: String,
    val firstName: String,
    val lastName: String,
    val userName: String,
    val persona: String,
    val mobileNumber: String,
    val userProfileImage: String,
    val createdBy: String,
    val modifiedBy: String,
    val createdAt: String,
    val modifiedAt: String
)

data class RoleResponseBody(
    val id: String,
    val role: String
)

data class AuthenticationResponse(
    val user: User,
    val jwtToken: String
)

data class NotificationSettingsResponseBody(
    val id: String,
    val userId: String,
    val filters: FiltersResponseBody,
    val action: String,
    val zoneId: String,
    val cameras: List<CameraResponseBody>,
    val createdBy: String,
    val modifiedBy: String,
    val createdAt: String,
    val modifiedAt: String
)

data class FiltersResponseBody(
    val minNumOfViolations: Int,
    val maxNumOfViolations: Int
)

data class RawStatsResponse(
    val zone_id: String?,
    val stats: StatsResponse,
    val video_link: String,
    val from_time: LocalDateTime,
    val to_time: LocalDateTime
)

data class CameraNotificationSettingsResponseBody(
    val id: String,
    val zoneId: String,
    val cameraId: String,
    val filters: FiltersResponseBody,
    val action: String,
    val userId: String,
    val createdBy: String,
    val modifiedBy: String,
    val createdAt: String,
    val modifiedAt: String
)

data class StatsResponse(
    val violations: BigInteger,
    val total_violations: BigInteger,
    val people: BigInteger,
    val total_people: BigInteger
)

data class LatestRawStatsResponse(
    val zone_id: String?,
    val stats: StatsResponse,
    val video_link: Any,
    val from_time: ZonedDateTime,
    val to_time: ZonedDateTime
)
