# Cogai Platform

This repository hosts several web services of the ONE App Chatbot Platform.

```shell
.
├── admin-service   | the backend for the cogai platform Admin web application
└── dashboard-service     | the backend for the cogai platform Dashboard web application
```

## Kickstart

You'll need java installed on your machine and setup on your `PATH`

```shell
# 0. bootstrap local IDE setup
$ ./bootstrap.sh
# 1. build binaries
$ ./gradlew clean build
# 2. run service locally
# Admin Service
java -jar -Dspring.profiles.active=local admin-service/build/libs/*.jar
# or Dashboard Service
java -jar -Dspring.profiles.active=local dashboard-service/build/libs/*.jar
```

## Debugging

```shell
# run locally with JVM listening for debugger to connect on port 5005
java \
-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 \ # depending on your shell you may need to escape the *
-jar path/to/jar/*.jar \
-Dspring.profiles.active=local
```

## Ktlint:
```shell
./gradlew ktlintApplyToIdea (Restart IDE after this)
./gradlew ktlintFormat
./gradlew ktlintCheck
./gradlew addKtlintCheckGitPreCommitHook
```
