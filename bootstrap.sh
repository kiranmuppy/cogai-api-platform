#/bin/bash

which ktlint &> /dev/null
if [ $? -ne 0 ]; then
  echo "linking project ktlint to /usr/local/bin/ktlint"
  ln -s $(pwd)/ktlint-0.37.1 /usr/local/bin/ktlint
  ls -al /usr/local/bin/ktlint
fi

ktlint applyToIDEAProject -y
ktlint installGitPreCommitHook
ktlint installGitPrePushHook